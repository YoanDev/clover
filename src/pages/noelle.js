import React, {useState} from 'react'
import HeroSectionNoelle from '../components/Noelle/HeroSection'
import Navbar from '../components/Navbar'
import Footer from '../components/Footer/index'
import Sidebar from '../components/Sidebar/'

const Noelle = () => {
    const [isOpen, setIsOpen] = useState(false)
    const toggle = ()=>{
        setIsOpen(!isOpen)
    }

    return (
        <div>
            <Navbar toggle={toggle}/>
            <HeroSectionNoelle />
            <Footer />
        </div>
    )
}

export default Noelle
