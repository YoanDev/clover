import React, {useState} from 'react'
import HeroSectionYami from '../components/Yami/HeroSection'
import Navbar from '../components/Navbar'
import Footer from '../components/Footer/index'
import Sidebar from '../components/Sidebar/'

const Yami = () => {
    const [isOpen, setIsOpen] = useState(false)
    const toggle = ()=>{
        setIsOpen(!isOpen)
    }

    return (
        <div>
            <Navbar toggle={toggle}/>
            <HeroSectionYami />
            <Footer />
        </div>
    )
}

export default Yami
