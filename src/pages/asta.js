import React, {useState} from 'react'
import HeroSectionAsta from '../components/Asta/HeroSection'
import Navbar from '../components/Navbar'
import Footer from '../components/Footer/index'
import Sidebar from '../components/Sidebar/'

const Asta = () => {
    const [isOpen, setIsOpen] = useState(false)
    const toggle = ()=>{
        setIsOpen(!isOpen)
    }

    return (
        <div>
            <Navbar toggle={toggle}/>
            <HeroSectionAsta />
            <Footer />
        </div>
    )
}

export default Asta
