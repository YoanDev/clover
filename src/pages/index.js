import React, {useState} from 'react'
import HeroSection from '../components/HeroSection'
import Navbar from '../components/Navbar'
import Footer from '../components/Footer/index'
import Sidebar from '../components/Sidebar/'

const Home = () => {
    const [isOpen, setIsOpen] = useState(false)
    const toggle = ()=>{
        setIsOpen(!isOpen)
    }

    return (
        <div>
            <Navbar />
            <HeroSection />
            <Footer />
        </div>
    )
}

export default Home
