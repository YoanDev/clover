import React, {useState} from 'react'
import HeroSectionYuno from '../components/Yuno/HeroSection'
import Navbar from '../components/Navbar'
import Footer from '../components/Footer/index'
import Sidebar from '../components/Sidebar/'

const Yuno = () => {
    const [isOpen, setIsOpen] = useState(false)
    const toggle = ()=>{
        setIsOpen(!isOpen)
    }

    return (
        <div>
            <Navbar toggle={toggle}/>
            <HeroSectionYuno />
            <Footer />
        </div>
    )
}

export default Yuno
