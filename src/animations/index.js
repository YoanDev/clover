export const animationOne = {
    in:{
        opacity:0,
        y:-500,
        background: '#FFF'
    },
    out:{
        opacity:1,
        y:500,
        background:'red',
    },
    end:{
        y:0,
        opacity:1,
        background:'#8A2BE2',
    },
}

export const animationYuno = {
    hidden:{
        background: '#FFF',
        color:'#010606'
    },
    visible:{
        opacity:1,
        background:'#161623',
        color: '#fff',
        transition: { duration:1, }
    },
    exit:{
        y:0,
        opacity:1,
        background:'#8A2BE2',
    },

    
      
}


export const ecussonVariants = {
    hover:{
        scale: 1.1,
        textShadow: "0px 0px 8px rgb(255, 255, 255)",
        boxShadow: "0px 0px 8px rgb(255, 255, 255)",
        transition:{
            duration: 0.3,
            yoyo: Infinity
        }
    }
}

export const pathVariants = {
    hidden: {
      opacity: 0,
      pathLength: 0
    },
    visible: {
      opacity: 1,
      pathLength: 1,
      transition: {
        duration: 2,
        ease: 'easeInOut'
      }
    },
  }

  export const slideVariants = {
      tap:{
        opacity:0.5
      },
  }