import './App.css';
import Home from './pages';
import {BrowserRouter as Link, Route, Switch, useLocation} from 'react-router-dom'
import {AnimatePresence} from 'framer-motion'
import Yami from './pages/yami';
import Asta from './pages/asta';
import Yuno from './pages/yuno';
import Noelle from './pages/noelle';

function App() {
  let location = useLocation()
  return (
    <>
    
        <Link>
          <AnimatePresence exitBeforeEnter>
            <Switch >
          <Route path='/' component={Home} exact />
          <Route path='/yami' component={Yami} exact />
          <Route path='/asta' component={Asta} exact />
          <Route path='/yuno' component={Yuno} exact />
          <Route path='/noelle' component={Noelle} exact />

            </Switch>
            
          </AnimatePresence>
          
      
      </Link>
    
    </>
  );
}

export default App;
