import Aos from 'aos'
import 'aos/dist/aos.css'
import React, {useEffect, useState} from 'react'
import { MobileIcon, Nav, NavbarContainer, NavBtn, NavBtnLink, NavItems, NavLinks, NavLogo, NavMenu } from './NavbarElements'
import {FaBars} from 'react-icons/fa'
import {animateScroll as scroll} from 'react-scroll'


const Navbar = ({toggle}) => {

    const [scrollNav, setScrollNav] = useState(false)

    const changeNav = ()=>{
        if(window.scrollY >= 80){
            setScrollNav(true)
        }else{
            setScrollNav(false)
        }
    }

    const toggleHome = ()=>{
        scroll.scrollToTop();
    }

    useEffect(()=>{
        window.addEventListener('scroll', changeNav)
    }, [])

    


  
    return (
        <Nav>
            <NavbarContainer data-aos='fade-left'>
                
                <NavLogo to='/' onClick={toggleHome}> Black Clover Y  </NavLogo>
                <MobileIcon >
                    <FaBars />
                </MobileIcon>
                <NavMenu>
                    <NavItems>
                        <NavLinks to='/yami'>Yami</NavLinks>
                    </NavItems>
                    <NavItems>
                        <NavLinks to='/yuno' smooth={true} duration={500} spy={true} exact='true' offset={-80}>Yuno</NavLinks>
                    </NavItems>
                    <NavItems>
                        <NavLinks to='/asta' smooth={true} duration={500} spy={true} exact='true' offset={-80}>Asta</NavLinks>
                    </NavItems>
                    <NavItems>
                        <NavLinks to='/noelle' smooth={true} duration={500} spy={true} exact='true' offset={-80}>Noelle</NavLinks>
                    </NavItems>
                    
                        
                </NavMenu>

               
            </NavbarContainer>
        </Nav>
    )
}

export default Navbar
