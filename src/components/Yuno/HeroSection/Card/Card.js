import Aos from 'aos'
import 'aos/dist/aos.css'
import React, {useState, useEffect} from 'react'
import {motion, Transition} from 'framer-motion'
import { animationYuno, ecussonVariants} from '../../../../animations'
import { Card, CardContainer, CardItems, Ecusson, H2Card, H3Card, PCard } from './CardElements'



const CardYuno = ({people}) => {
    const [ReadMore, SetReadMore] = useState(false)
    const [index, setIndex] = useState(0)
    const {id, image, text, theme, title, statut, paragraphe, ecusson} = people[index]
    const variants = {
        hidden: { opacity: 0 },
        visible: { opacity: 1 },
      }
    const checkNumber = (number) =>{
        if(number > people.length -1){
            return 0
        }
        if(number < 0){
            return people.length - 1
        }
        return number
    }
    
    const nextPerson = () => {
        setIndex(()=>{
            let newIndex = index + 1
            return checkNumber(newIndex)
        })
    }

    const prevPerson = () => {
        setIndex(()=>{
            let newIndex = index - 1
            return checkNumber(newIndex)
        })
    }

    useEffect(()=>{
        Aos.init({duration:2000})
    }, [])

    function changeTheme (theme){
        document.body.style.backgroundColor = 'red'
    }

    
            
                return (
                    
                    <motion.div
                    variants={animationYuno}
                    initial="hidden"
                    animate="visible"
                    exit="exit"
                    >
                    
                    <CardContainer data-aos='fade-left' id={id}
                   
                    >
                        <Card
                        variants={animationYuno}
                        initial="hidden"
                        animate="visible"
                        exit="exit"
                        drag
                        dragConstraints={{top:0, left:0, right:0, bottom:0}}
                        >
                            <Ecusson src={ecusson}
                            variants={ecussonVariants}
                            whileHover='hover'
                            drag
                            dragConstraints={{top:0, left:0, right:0, bottom:0}}
                            whileTap={{ scale: 1.3 }}
                            

                            />
                            <CardItems>
                                <H2Card>{title} </H2Card>
                                <H3Card>{statut} </H3Card>
                                <PCard>{paragraphe} </PCard>
                            </CardItems>
                        </Card>
                    </CardContainer>
                
                </motion.div>
                )

            
        
    
}

export default CardYuno
