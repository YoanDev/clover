import styled from 'styled-components'
import { motion } from 'framer-motion'


export const CardContainer = styled.div`
    position: absolute;
    top:250px;
    right:700px;
    display:grid;
    flex-wrap: wrap;
    z-index: 1;
    grid-template-columns: 1fr 1fr 1fr;
    background:#161623;


    

`

export const Card = styled(motion.div)`
    position: absolute;
    width: 380px;
    height: 500px;
    margin:30px;
    box-shadow: 20px 20px 50px rgba(0, 0, 0, 0.5);
    border-radius: 15px;
    background:rgba(255, 255, 255, 0.1);
    display:flex;
    justify-content: center;
    align-items: center;
    border-top: 1px solid rgba(255, 255, 255, 0.5);
    border-left: 1px solid rgba(255, 255, 255, 0.5);
    padding: 16px;
    backdrop-filter: blur(5px);


    @media screen and (max-width:1000px){
        left:200px;
        width:300px;
    }
    @media screen and (max-width:768px){
        left:310px;
        width:300px;
    }
    @media screen and (max-width:568px){
        left:425px;
        width:200px;
    }
`

export const Ecusson = styled(motion.img)`
 width:100px;
 height:110px;
 position: absolute;
 top:0;
 
`

export const CardItems = styled.div`
    opacity: 1;
    transition: all 0.4s ease-in-out;
    pointer-events: none;
`


export const H2Card = styled.h2`
 color:#fff;
 margin-bottom:35px;
 font-size:32px;
`


export const H3Card = styled.h3`
 font-size:16px;
 color:#8A2BE2;
`

export const PCard = styled.p`
 font-size:16px;
 color:#fff;
`