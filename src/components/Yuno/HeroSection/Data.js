import Bg1 from '../../../images/yuno4.jpg'
import Bg2 from '../../../images/yuno2.jpg'
import Bg3 from '../../../images/yuno3.jpg'

import Ecusson from '../../../images/ecusson-aubedor.png'



export default [
    {
        id:0,
        image: Bg1,
        text:'Welcome in Clover Kingdom of Magic',
        theme:'theme1',
        title:'Yuno',
        statut:'Membre de l\'aube d\'or',
        paragraphe:'Yuno est le rival du héros de l\'histoire Asta. Ils grandirent ensemble dans la campagne au sein d\'une église, leur objectif commun est de devenir Empereur Mage afin de prouver qu\'un gueux aussi puisse réussir',
        ecusson: Ecusson,
    },
    {
        id:1,
        image: Bg2,
        text:'Welcome in Clover Kingdom of Magic',
        theme:'theme1',
        title:'Asta',
        statut:'Membre du Taureau Noir',
        paragraphe:'Héros de l\' histoire il est la personne la plus déterminée qui existe dans Black Clover. Né sans magie il s\'entraîne sans relâche pour devenir Empereur Mage',
        ecusson: Ecusson,
    },
    {
        id:2,
        image: Bg3,
        text:'Welcome in Clover Kingdom of Magic',
        theme:'theme1',
        title:'Asta',
        statut:'Membre du Taureau Noir',
        paragraphe:'Héros de l\' histoire il est la personne la plus déterminée qui existe dans Black Clover. Né sans magie il s\'entraîne sans relâche pour devenir Empereur Mage',
        ecusson: Ecusson,
    },
    
    
   
   
]