import React, { useState } from 'react'

import List from './List'
import Data from './Data'
import CardYuno from './Card/Card'

const HeroSectionYuno = (id, image, text) => {
    const [hover, setHover] = useState(false)
    const [people, setPeople] = useState(Data)


    const onHover = () => {
        setHover(!hover)
    }

    

    return (
        <>
        <List  people={people} />
        <CardYuno people={people} />
        </>
    )
}

export default HeroSectionYuno
