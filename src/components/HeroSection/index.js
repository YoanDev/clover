import React, { useState } from 'react'

import List from './List'
import Data from './Data'

const HeroSection = (id, image, text) => {
    const [hover, setHover] = useState(false)
    const [people, setPeople] = useState(Data)


    const onHover = () => {
        setHover(!hover)
    }

    

    return (
        
        <List  people={people} />
    )
}

export default HeroSection
