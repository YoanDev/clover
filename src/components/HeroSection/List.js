import Aos from 'aos'
import 'aos/dist/aos.css'
import React, {useState, useEffect} from 'react'
import {  HeroBg, HeroContainer, HeroContent, HeroH1, HeroP, ImageBg } from './HeroElements'
import {AnimatePresence, motion, Transition} from 'framer-motion'
import {animationOne} from '../../animations'


const List = ({people}) => {
    const [ReadMore, SetReadMore] = useState(false)
    const [index, setIndex] = useState(0)
    const {id, image, text, theme} = people[index]

    const checkNumber = (number) =>{
        if(number > people.length -1){
            return 0
        }
        if(number < 0){
            return people.length - 1
        }
        return number
    }
    
    const nextPerson = () => {
        setIndex(()=>{
            let newIndex = index + 1
            return checkNumber(newIndex)
        })
    }

    const prevPerson = () => {
        setIndex(()=>{
            let newIndex = index - 1
            return checkNumber(newIndex)
        })
    }

   
    useEffect(()=>{
        Aos.init({duration:2000})
    }, [])

    function changeTheme (theme){
        document.body.style.backgroundColor = 'red'
    }

    
            
                return (
                    <AnimatePresence exitBeforeEnter>
                    <motion.div
                    initial="in"
                    animate='end'
                    exit='out'
                    variants={animationOne}
                    >
                    <HeroContainer  data-aos='fade-zoom-in' onClick={nextPerson} onChange={changeTheme}  id={id}
                    initial={{ opacity:0.8 }}
                    whileTap={{ scale:0.2, opacity:0.2 }}

                    
                    >
                        <HeroBg
                        initial={{opacity: 0}}
                        animate={{ opacity: 1, transition:{duration:1.5}}}
                        whileTap={{ scale:0.2 }}

                        
                        id={theme} >

                            <ImageBg  src={image} />
                        </HeroBg>
                        <HeroContent>
                            <HeroH1 data-aos='fade-zoom-in'>{} </HeroH1>
                            <HeroP></HeroP>
                        
                            
                        </HeroContent>
                </HeroContainer>
                
                </motion.div>
                </AnimatePresence>
                )

            
        
    
}

export default List
