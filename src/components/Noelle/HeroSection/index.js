import React, { useState } from 'react'

import List from './List'
import Data from './Data'
import CardNoelle from './Card/Card'

const HeroSectionNoelle = (id, image, text) => {
    const [hover, setHover] = useState(false)
    const [people, setPeople] = useState(Data)


    const onHover = () => {
        setHover(!hover)
    }

    

    return (
        <>
        <List  people={people} />
        <CardNoelle people={people} />
        </>
    )
}

export default HeroSectionNoelle
