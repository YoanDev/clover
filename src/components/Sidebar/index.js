import React from 'react'
import {CloseIcon, Icon, SidebarContainer,  SidebarWrapper, SidebarLink, SidebarMenu, SidebarBg} from './SidebarElements'
import SideBg from '../../images/asta1.jpg'

const Sidebar = ({isOpen, toggle}) => {
    return (
        <SidebarContainer isOpen={isOpen} onClick={toggle}>
            <SidebarBg src={SideBg} />
            <Icon onClick={toggle}>
                <CloseIcon />
            </Icon>
            <SidebarWrapper>
                <SidebarMenu>
                <SidebarLink  to='/' onClick={toggle}>
                        Home
                    </SidebarLink>
                    <SidebarLink  to='/yami' onClick={toggle}>
                        Yami
                    </SidebarLink>
                    <SidebarLink to='/yuno' onClick={toggle}>
                        Yuno
                    </SidebarLink> 
                    <SidebarLink to='/asta' onClick={toggle}>
                        Asta
                    </SidebarLink>
                    <SidebarLink to='/noelle' onClick={toggle}>
                        Noelle
                    </SidebarLink>
                </SidebarMenu>
                
            </SidebarWrapper>
        </SidebarContainer>
    )
}

export default Sidebar
