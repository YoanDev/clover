import React, { useState } from 'react'

import List from './List'
import Data from './Data'
import CardYami from './Card/Card'

const HeroSectionYami = (id, image, text) => {
    const [hover, setHover] = useState(false)
    const [people, setPeople] = useState(Data)


    const onHover = () => {
        setHover(!hover)
    }

    

    return (
        <>
        <List  people={people} />
        <CardYami people={people} />
        </>
    )
}

export default HeroSectionYami
