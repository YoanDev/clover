import React, { useState } from 'react'

import List from './List'
import Data from './Data'
import CardAsta from './Card/Card'

const HeroSectionAsta = (id, image, text) => {
    const [hover, setHover] = useState(false)
    const [people, setPeople] = useState(Data)


    const onHover = () => {
        setHover(!hover)
    }

    

    return (
        <>
        <List  people={people} />
        <CardAsta people={people} />
        </>
    )
}

export default HeroSectionAsta
