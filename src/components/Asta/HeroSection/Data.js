import Bg1 from '../../../images/asta2.jpg'
import Bg2 from '../../../images/asta3.jpg'
import Bg3 from '../../../images/asta4.jpg'
import Bg4 from '../../../images/asta1.jpg'
import Ecusson from '../../../images/ecusson-taureaunoir.png'



export default [
    {
        id:0,
        image: Bg1,
        text:'Welcome in Clover Kingdom of Magic',
        theme:'theme1',
        title:'Asta',
        statut:'Membre du Taureau Noir',
        paragraphe:'Héros de l\' histoire il est la personne la plus déterminée qui existe dans Black Clover. Né sans magie il s\'entraîne sans relâche pour devenir Empereur Mage',
        ecusson: Ecusson,
    },
    {
        id:1,
        image: Bg2,
        text:'Welcome in Clover Kingdom of Magic',
        theme:'theme1',
        title:'Asta',
        statut:'Membre du Taureau Noir',
        paragraphe:'Héros de l\' histoire il est la personne la plus déterminée qui existe dans Black Clover. Né sans magie il s\'entraîne sans relâche pour devenir Empereur Mage',
        ecusson: Ecusson,
    },
    {
        id:2,
        image: Bg3,
        text:'Welcome in Clover Kingdom of Magic',
        theme:'theme1',
        title:'Asta',
        statut:'Membre du Taureau Noir',
        paragraphe:'Héros de l\' histoire il est la personne la plus déterminée qui existe dans Black Clover. Né sans magie il s\'entraîne sans relâche pour devenir Empereur Mage',
        ecusson: Ecusson,
    },
    {
        id:3,
        image: Bg4,
        text:'Welcome in Clover Kingdom of Magic',
        theme:'theme1',
        title:'Asta',
        statut:'Membre du Taureau Noir',
        paragraphe:'Héros de l\' histoire il est la personne la plus déterminée qui existe dans Black Clover. Né sans magie il s\'entraîne sans relâche pour devenir Empereur Mage',
        ecusson: Ecusson,
    },
    
   
   
]